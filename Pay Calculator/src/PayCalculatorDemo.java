/**
 * 
 * @author Ahsan Zaman
 *
 * Algorithm:
 * 1. Ask user for pay rate.
 * 2. Ask user for hours worked.
 * 3. Ask user if they want to calculate regular or hazard pay.
 * 4. Call appropriate constructor using polymorphism and pass pay rate as argument.
 * 5. Use computePay method with hours as argument to calculate pay.
 * END
 */

import javax.swing.*;
public class PayCalculatorDemo {
	
	public enum Menu{
		EXIT, REGULAR_PAY, HAZARD_PAY;
	}
	
	public static void main( String[] args ){
		/**Taking Inputs
		 * payRate - It is the hourly rate the person is being paid.
		 * hours - Hours worked by the person
		 * payObject - Changed into a RegularPay object or HazardPay based on user's choice.
		 * */
		float payRate = Float.parseFloat( JOptionPane.showInputDialog( "Enter the pay rate: " ) );
		float hours = Float.parseFloat( JOptionPane.showInputDialog( "Enter the hours worked: " ) );
		PayCalculator payObject;
		
		int choice = Integer.parseInt( JOptionPane.showInputDialog( "Choose one of the following options or press 0 to exit: "
				+ "\n1. Calculate Regular Pay\n2. Calculate Hazard Pay" ) );
		switch( Menu.values()[choice] ){
		case EXIT:
			JOptionPane.showMessageDialog( null , "Exiting Program." );
			break;
		case REGULAR_PAY:
			payObject = new RegularPay( payRate );
			JOptionPane.showMessageDialog( null, "Regular Pay Will be: "+payObject.computePay( hours ) );
			break;
		case HAZARD_PAY:
			payObject = new HazardPay( payRate );
			JOptionPane.showMessageDialog( null, "Hazard Pay Will be: "+payObject.computePay( hours ) );
			break;
			default:
				JOptionPane.showMessageDialog( null, "Incorrect entry. " );		
		}
	}
}

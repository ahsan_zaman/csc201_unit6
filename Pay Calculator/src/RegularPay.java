
public class RegularPay extends PayCalculator{
	
	/**
	 * 
	 * @param payRate - Rate which the employee is being paid hourly.
	 */
	public RegularPay( float payRate ){
		this.payRate = payRate;
	}
}


public class HazardPay extends PayCalculator{
	/**
	 * 
	 * @param payRate - Rate which the employee is being paid hourly.
	 */
	public HazardPay( float payRate ){
		this.payRate = payRate;
	}
	
	/**
	 * @param hours - The hours worked by the employee.
	 * @return The pay calculated based on hours worked, payrate and a constant 1.5.
	 */
	public float computePay( float hours ){
		return payRate*hours*1.5f;
	}
}

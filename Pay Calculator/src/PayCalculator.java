
public abstract class PayCalculator {
	protected float payRate;
	
	/**
	 * 
	 * @param hours - Computes the pay that the employee will get based the pay rate and hours worked.
	 * @return The pay calculated.
	 */
	public float computePay( float hours ){
		return payRate*hours;
	}
	
}


public interface MessageEncoder {
	/**
	 * 
	 * @param plainText - Takes the plain text to encode.
	 * @return The cipher text is returned,
	 */
	public abstract StringBuilder encode( String plainText );
}

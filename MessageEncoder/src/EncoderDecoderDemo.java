/**
 * @author Ahsan Zaman
 * CSC 201 - Unit 6
 * Encoder Decoder Problem
 * 
 * Algorithm: 
 * 1. Ask user for String input for encoding/decoding.
 * 2. Give user menu choices for encoding/decoding using different ciphers.
 * 3. Create either SubstitutionCipher or Shuffle cipher objects as per user's wishes.
 * 4. Call encode or decode appropriately.
 * 5. Display Translated text.
 * END
 */
import javax.swing.*;
public class EncoderDecoderDemo {
	public enum Menu{
		EXIT, ENCODE_SUBSTITUTION, ENCODE_SHUFFLE, DECODE_SUBSTITUTION, DECODE_SHUFFLE;
	}
	public static void main( String[] args ){
		String plainText = JOptionPane.showInputDialog( "Enter plain text or cipher text for translation: " );
		int choice = Integer.parseInt( JOptionPane.showInputDialog( "Choose a cipher to perform or enter 0 to exit:"
				+ "\n1. Encode by Substitution Cipher\n2. Encode by Shuffle Cipher"
				+ "\n3. Decode by Substitution Cipher\n4. Decode by Shufle Cipher" ) );
		
		switch( Menu.values()[choice] ) {
		case EXIT:
			JOptionPane.showMessageDialog( null, "Exiting Program" );
			break;
		case ENCODE_SUBSTITUTION:
			SubstitutionCipher encodeSubstitution = new SubstitutionCipher( Integer.parseInt( JOptionPane.showInputDialog( "Enter the constant to shift the plain text by: " ) ) );
			JOptionPane.showMessageDialog( null, "Encoded Message: "+encodeSubstitution.encode( plainText ) );
			break;
		case ENCODE_SHUFFLE:
			ShuffleCipher encodeShuffle = new ShuffleCipher( Integer.parseInt( JOptionPane.showInputDialog( "Enter the number of times to shuffle the plain text: " ) ) );
			JOptionPane.showMessageDialog( null, "Encoded Message: "+encodeShuffle.encode( plainText ) );
			break;
		case DECODE_SUBSTITUTION:
			SubstitutionCipher decodeSubstitution = new SubstitutionCipher( Integer.parseInt( JOptionPane.showInputDialog( "Enter the shift constant: " ) ) );
			JOptionPane.showMessageDialog( null, "Decoded Message: "+decodeSubstitution.decode( plainText ) );
			break;
		case DECODE_SHUFFLE:
			ShuffleCipher decodeShuffle = new ShuffleCipher( Integer.parseInt( "Enter the number of times to decode: " ) );
			JOptionPane.showMessageDialog( null, "Decoded Message: "+decodeShuffle.decode( plainText ) );
			break;
			
		}
		
	}
}

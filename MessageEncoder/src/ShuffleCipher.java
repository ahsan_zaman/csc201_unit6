import javax.swing.JOptionPane;


public class ShuffleCipher implements MessageEncoder, MessageDecoder{
	public int n;
	
	/**
	 * 
	 * @param n - Number of times to shuffle the text.
	 */
	public ShuffleCipher( int n ){
		this.n = n;
	}
	
	@Override
	public StringBuilder encode(String plainText) {
		// TODO Auto-generated method stub
		StringBuilder cipherText = new StringBuilder( plainText );
		
		for( int i=0; i<this.n ; i++){
			
			String firstHalf=shuffler( cipherText, 0, ( cipherText.length()/2 )+1 );
			String secondHalf=shuffler( cipherText, ( plainText.length()/2 )+1, plainText.length() );
			JOptionPane.showMessageDialog( null, firstHalf+"\n"+secondHalf+"\n"+firstHalf.charAt( 0 ) );
			
			for( int j=0; j<cipherText.length() ;j++ ){
				if( j==0 | (j%2)==0 )
					cipherText.setCharAt( j, firstHalf.charAt( j ) );
				else 
					cipherText.setCharAt( j, secondHalf.charAt( j ) );
			}
		}
		return cipherText;
	}

	@Override
	public StringBuilder decode(String plainText) {
		// TODO Auto-generated method stub
		StringBuilder cipherText = new StringBuilder( plainText );
		for( int i=0; i<this.n ; i++){
			for( int j=0; j<plainText.length() ;j++ ){
				if( j==0 | (j%2)==0 )
					cipherText.setCharAt( j, shuffler( cipherText, plainText.length()/2, plainText.length() ).charAt( j ) );
				else 
					cipherText.setCharAt( j, shuffler( cipherText, 0, plainText.length()/2 ).charAt( j ) );
			}
		}
		return cipherText;
	}
	
	/**
	 * 
	 * @param plainText - The text to split in half.
	 * @param index1 - The starting index for the split.
	 * @param index2 - The ending index for the split.
	 * @return Half of the plainText
	 */
	public String shuffler( StringBuilder plainText, int index1, int index2 ){
		return plainText.substring( index1, index2 );
	}
	
}

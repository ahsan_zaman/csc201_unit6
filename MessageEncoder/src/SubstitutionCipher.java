
public class SubstitutionCipher implements MessageEncoder, MessageDecoder {
	private int shift;
	
	/**
	 * 
	 * @param shift - Number by which to shift each character by.
	 */
	public SubstitutionCipher( int shift ) {
		this.shift = shift;
	}

	@Override
	
	public StringBuilder encode(String plainText) {
		StringBuilder cipherText= new StringBuilder( plainText );
		for( int i=0; i<plainText.length() ;i++ ){
			cipherText.setCharAt( i, encodeShifter( plainText.charAt(i) ) );
		}
		return cipherText;
	}

	@Override
	public StringBuilder decode(String plainText) {
		// TODO Auto-generated method stub
		StringBuilder cipherText= new StringBuilder( plainText );
		for( int i=0; i<plainText.length() ;i++ ){
			cipherText.setCharAt( i, decodeShifter( plainText.charAt(i) ) );
		}
		return cipherText;
	}
	
	/**
	 * 
	 * @param letter - Letter to shift.
	 * @return Shifted letter is returned.
	 */
	private char encodeShifter( char letter ) {
		return (char)(letter+this.shift);
	}
	
	/**
	 * 
	 * @param letter - Letter to shift back to find original one.
	 * @return Original Letter is returned.
	 */
	private char decodeShifter( char letter ) {
		return (char)(letter-this.shift);
	}
}

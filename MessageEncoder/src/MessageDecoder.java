
public interface MessageDecoder {
	/**
	 * 
	 * @param plainText - Takes the text to decode. 
	 * @return The decoded text.
	 */
	public abstract StringBuilder decode( String plainText );
}
